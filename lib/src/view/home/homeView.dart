import 'package:flutter/material.dart';
import 'package:flutter_expense_tracker/src/bloc/expenseBloc.dart';
import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:flutter_expense_tracker/src/dataModels/spentModel.dart';
import 'package:flutter_expense_tracker/src/view/addExpese/addExpense.dart';
import 'package:flutter_expense_tracker/src/view/detailsView/detailsView.dart';
import 'package:intl/intl.dart';

class HomeView extends StatefulWidget {
  final String title;

  HomeView({Key key, this.title}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  DateTime currentDate = new DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);
  ExpenseBloc expenseBloc;

  @override
  void initState() {
    super.initState();
    expenseBloc = ExpenseBloc(currentDate.toIso8601String());
  }

  @override
  void dispose() {
    super.dispose();
    expenseBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          actions: <Widget>[
            IconButton(
              onPressed: showBottomSheet,
              icon: Icon(
                Icons.calendar_today,
              ),
              tooltip: 'Select Day',
            ),
          ],
        ),
        body: getExpenseWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: addExpense,
        ),
      ),
    );
  }

  Widget getExpenseWidget() {
    return StreamBuilder(
      stream: expenseBloc.amountModels,
      builder:
          (BuildContext context, AsyncSnapshot<List<AmountModel>> snapshot) {
        return getExpenseList(snapshot);
      },
    );
  }

  Widget getExpenseList(AsyncSnapshot<List<AmountModel>> snapshot) {
    if (snapshot.hasData) {
      return Column(
        children: <Widget>[
          Expanded(
            child: snapshot.data.length > 0
                ? ListView.builder(
                    cacheExtent: 1,
                    addAutomaticKeepAlives: false,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      String amount =
                          snapshot.data[index].isExpense ? '-' : '+';
                      amount =
                          amount + (snapshot.data[index].amount.toString());
                      return Padding(
                        padding: const EdgeInsets.only(
                          left: 8.0,
                          right: 8.0,
                        ),
                        child: IntrinsicHeight(
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: VerticalDivider(
                                  color: Colors.white,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 8.0,
                                  bottom: 8.0,
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        snapshot.data[index].source,
                                      ),
                                    ),
                                    Expanded(
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Text(
                                          amount,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  )
                : showNoDataWidget(),
          ),
          Divider(),
          buildTotalView(),
        ],
      );
    } else {
      return Center(
        child: loadingData(),
      );
    }
  }

  Widget loadingData() {
    //pull todos again
    expenseBloc.getAmount();
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Text("Loading...",
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  Widget showNoDataWidget() {
    return Container(
      child: Center(
        child: Text(
          "Start by adding Expenses...",
          style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  void addExpense() async {
    String toDate = DateFormat('dd MMM, yyyy').format(currentDate);
    await showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          elevation: 100,
          title: Text('Adding info for $toDate'),
          children: <Widget>[
            AddExpense(
              selectedDate: currentDate,
              expenseBloc: expenseBloc,
            ),
          ],
        );
      },
    );
  }

  Widget buildTotalView() {
    SpentModel today = expenseBloc.today;
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Today Expense',
              ),
              Text(
                today.todaySpent.toString(),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText1.fontSize + 10,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Today Earning',
              ),
              Text(
                today.todayEarned.toString(),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText1.fontSize + 10,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(
            10.0,
          ),
          child: Align(
            alignment: Alignment.centerLeft,
            child: OutlineButton(
              onPressed: () {
                showMoreDetails();
              },
              child: Text('Show more data'),
            ),
          ),
        ),
      ],
    );
  }

  void showBottomSheet() async {
    currentDate = await showDatePicker(
      context: context,
      initialDate: currentDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day + 1,
      ),
    );
    if (currentDate == null) {
      currentDate = DateTime.now();
    } else {
      expenseBloc.changeDay(currentDate.toIso8601String());
    }
  }

  void showMoreDetails() async {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => MoreDetailsDialog(
          totalSpentModel: expenseBloc.allTime,
          bloc: expenseBloc,
        ),
      ),
    );
  }
}
