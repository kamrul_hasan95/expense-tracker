import 'package:flutter/material.dart';
import 'package:flutter_expense_tracker/src/bloc/expenseBloc.dart';
import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';

class AddExpense extends StatefulWidget {
  final DateTime selectedDate;
  final ExpenseBloc expenseBloc;

  AddExpense({this.selectedDate, this.expenseBloc});

  @override
  _AddExpenseState createState() => _AddExpenseState();
}

class _AddExpenseState extends State<AddExpense> {
  TextEditingController controllerAmount = new TextEditingController();
  TextEditingController controllerSource = new TextEditingController();
  bool isExpense = true;
  String amountErrorText = '';
  String sourceErrorText = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(
        8.0,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            FlatButton.icon(
              color: Colors.white10,
              onPressed: () {
                setState(() {
                  isExpense = !isExpense;
                });
              },
              icon: isExpense ? Icon(Icons.remove) : Icon(Icons.add),
              label: Text(
                isExpense ? "Expense" : "Earning",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                keyboardType: TextInputType.number,
                controller: controllerAmount,
                decoration: InputDecoration(
                  labelText: 'Amount',
                  errorText:
                      amountErrorText.length > 1 ? amountErrorText : null,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      12.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controllerSource,
                decoration: InputDecoration(
                  labelText: 'Source',
                  errorText:
                      sourceErrorText.length > 1 ? sourceErrorText : null,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      12.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: OutlineButton(
                onPressed: save,
                child: Text(
                  'Add',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void save() {
    if (validateAmount() | validateSource()) {
      AmountModel amountModel = new AmountModel(
        day: widget.selectedDate,
        amount: (double.parse(controllerAmount.text)).abs(),
        isExpense: isExpense,
        source: controllerSource.text,
      );

      widget.expenseBloc.addAmount(amountModel);
      Navigator.of(context).pop();
    }
  }

  bool validateAmount() {
    if (controllerAmount != null &&
        controllerAmount.text != null &&
        controllerAmount.text.length > 0) {
      try {
        double.parse(controllerAmount.text);
        setState(() {
          amountErrorText = '';
        });
        return true;
      } on FormatException catch (e) {
        setState(() {
          amountErrorText = 'Please enter valid number';
        });
        return false;
      }
    } else {
      setState(() {
        amountErrorText = 'Please enter valid number';
      });
      return false;
    }
  }

  bool validateSource() {
    if (controllerSource != null &&
        controllerSource.text != null &&
        controllerSource.text.length > 0) {
      setState(() {
        sourceErrorText = '';
      });
      return true;
    } else {
      setState(() {
        sourceErrorText = 'Please enter valid source';
      });
      return false;
    }
  }
}
