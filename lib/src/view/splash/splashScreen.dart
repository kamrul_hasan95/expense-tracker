import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_expense_tracker/src/view/home/homeView.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    new Timer(
        new Duration(
          seconds: 1,
        ), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => HomeView(
            title: 'Home',
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'Expense Manager',
          style: TextStyle(
            fontSize: 30.0,
          ),
        ),
      ),
    );
  }
}
