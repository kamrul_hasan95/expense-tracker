import 'package:flutter/material.dart';
import 'package:flutter_expense_tracker/src/bloc/expenseBloc.dart';
import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:flutter_expense_tracker/src/dataModels/spentModel.dart';
import 'package:flutter_expense_tracker/src/db/expenseDao.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

class MoreDetailsDialog extends StatefulWidget {
  final ExpenseBloc bloc;
  final SpentModel totalSpentModel;

  MoreDetailsDialog({this.bloc, this.totalSpentModel});

  @override
  _MoreDetailsDialogState createState() => _MoreDetailsDialogState();
}

class _MoreDetailsDialogState extends State<MoreDetailsDialog> {
  bool isLoading = true;
  DateTime selectedDate = DateTime(DateTime.now().year, DateTime.now().month);
  String selectedDateString;
  List<AmountModel> amountModels;
  double expenseThisMonth = 0.0;
  double savingThisMonth = 0.0;
  double remainingAmount;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedDateString = DateFormat('MMM, yyyy').format(selectedDate);

    getData();
  }

  void getData() async {
    amountModels = await ExpenseDao().getAmounts();

    expenseThisMonth = 0.0;
    savingThisMonth = 0.0;

    for (int i = 0; i < amountModels.length; i++) {
      if (amountModels[i].day.year == selectedDate.year &&
          amountModels[i].day.month == selectedDate.month) {
        if (amountModels[i].isExpense) {
          expenseThisMonth += amountModels[i].amount;
        } else {
          savingThisMonth += amountModels[i].amount;
        }
      }
    }

    remainingAmount = savingThisMonth - expenseThisMonth;

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Showing data for $selectedDateString'),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(
                  8.0,
                ),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerRight,
                      child: OutlineButton(
                        onPressed: pickMonth,
                        child: Text('Pick Month'),
                      ),
                    ),
                    getExpenseRow('Expense this month', expenseThisMonth),
                    getExpenseRow('Earning this month', savingThisMonth),
                    getExpenseRow('Savings this month', remainingAmount),
                    Divider(
                      color: Colors.white,
                    ),
                    getExpenseRow(
                        'Expense all time', widget.totalSpentModel.todaySpent),
                    getExpenseRow(
                        'Earning all time', widget.totalSpentModel.todayEarned),
                    getExpenseRow(
                        'Savings all time',
                        widget.totalSpentModel.todayEarned -
                            widget.totalSpentModel.todaySpent),
                  ],
                ),
              ),
            ),
    );
  }

  Widget getExpenseRow(String text, double value) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            '$text',
          ),
          Text(
            value.toString(),
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.bodyText1.fontSize + 10,
            ),
          ),
        ],
      ),
    );
  }

  void pickMonth() async {
    DateTime selected = await showMonthPicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    );
    if (selected != null) {
      setState(() {
        isLoading = true;
      });
      selectedDate = selected;
      selectedDateString = DateFormat('MMM, yyyy').format(selectedDate);
      getData();
    }
  }
}
