import 'dart:async';

import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:flutter_expense_tracker/src/dataModels/spentModel.dart';
import 'package:flutter_expense_tracker/src/repository/expenseRepository.dart';

class ExpenseBloc {
  String day;
  final ExpenseRepository _expenseRepository = ExpenseRepository();
  SpentModel today;
  SpentModel allTime;

  final _expenseController = StreamController<List<AmountModel>>.broadcast();

  get amountModels => _expenseController.stream;

  changeDay(String day) {
    this.day = day;
    getAmount();
  }

  ExpenseBloc(this.day) {
    getAmount();
  }

  getAmount({int folderId}) async {
    List<AmountModel> amountModel =
        await _expenseRepository.getAllTodoAmounts(query: day);
    await getTotalSpentAmount();
    await getTodaySpentAmount();
    _expenseController.sink.add(amountModel);
  }

  getTodaySpentAmount() async {
    today = await _expenseRepository.getTodaySpentAmount(day);
  }

  getTotalSpentAmount() async {
    allTime = await _expenseRepository.getAllTimeSpentAmount();
  }

  addAmount(AmountModel amountModel) async {
    await _expenseRepository.insertAmount(amountModel);
    getAmount();
  }

  updateAmount(AmountModel amountModel) async {
    await _expenseRepository.updateAmount(amountModel);
    getAmount();
  }

  deleteAmountByID(int id) async {
    await _expenseRepository.deleteAmountByID(id);
    getAmount();
  }

  deleteAllAmountItems() async {
    await _expenseRepository.deleteAllAmount();
    getAmount();
  }

  dispose() {
    _expenseController.close();
  }
}
