final String columnId = 'id';
final String columnDay = 'day';
final String columnAmount = 'amount';
final String columnExpense = 'isExpense';
final String columnSource = 'source';

class AmountModel {
  int id;
  DateTime day;
  double amount;
  bool isExpense;
  String source;

  AmountModel({this.id, this.day, this.amount, this.isExpense, this.source});

  factory AmountModel.fromMap(Map<String, dynamic> json) => AmountModel(
        id: json[columnId],
        amount: json[columnAmount],
        day: DateTime.parse(json[columnDay]),
        isExpense: json[columnExpense] == 0 ? false : true,
        source: json[columnSource],
      );

  Map<String, dynamic> toMap() => {
        columnId: id,
        columnAmount: amount,
        columnDay: day.toIso8601String(),
        columnExpense: isExpense ? 1 : 0,
        columnSource: source,
      };

  @override
  String toString() {
    return 'AmountModel{id: $id, day: $day, amount: $amount, isExpense: $isExpense, source: $source}';
  }
}
