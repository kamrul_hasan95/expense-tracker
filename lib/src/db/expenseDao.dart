import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:flutter_expense_tracker/src/dataModels/spentModel.dart';
import 'package:flutter_expense_tracker/src/db/database.dart';

class ExpenseDao {
  final dbProvider = DatabaseProvider.dbProvider;

  // add new folder
  Future<int> addAmount(AmountModel amountModel) async {
    final db = await dbProvider.database;
    int result = await db.insert(expenseTable, amountModel.toMap());
    return result;
  }

  Future<List<AmountModel>> getAmounts({String query}) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> results;

    if (query != null && query.isNotEmpty) {
      results = await db.query(
        expenseTable,
        where: '$columnDay LIKE ?',
        whereArgs: ['%$query%'],
      );
    } else {
      results = await db.query(expenseTable);
    }
    List<AmountModel> amountModels = results.isNotEmpty
        ? results.map((item) {
            return AmountModel.fromMap(item);
          }).toList()
        : [];

    return amountModels;
  }

  Future<SpentModel> getTodayCalculation(String query) async {
    List<AmountModel> amountModels = await getAmounts(query: query);
    double totalExpense = 0, totalEarned = 0;
    for (int i = 0; i < amountModels.length; i++) {
      if (amountModels[i].isExpense) {
        totalExpense += amountModels[i].amount;
      } else {
        totalEarned += amountModels[i].amount;
      }
    }
    return SpentModel(
      todayEarned: totalEarned,
      todaySpent: totalExpense,
    );
  }

  Future<SpentModel> getTotalEarnedAndExpense() async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> results;

    results = await db.query(
      expenseTable,
    );

    List<AmountModel> amountModels = results.isNotEmpty
        ? results.map((item) => AmountModel.fromMap(item)).toList()
        : [];

    double totalExpense = 0, totalEarned = 0;
    for (int i = 0; i < amountModels.length; i++) {
      if (amountModels[i].isExpense) {
        totalExpense += amountModels[i].amount;
      } else {
        totalEarned += amountModels[i].amount;
      }
    }

    return SpentModel(
      todayEarned: totalEarned,
      todaySpent: totalExpense,
    );
  }

  Future<int> updateAmount(AmountModel amountModel) async {
    final db = await dbProvider.database;

    var result = await db.update(
      expenseTable,
      amountModel.toMap(),
      where: '$columnId = ?',
      whereArgs: [amountModel.id],
    );

    return result;
  }

  Future<int> deleteAmount(int id) async {
    final db = await dbProvider.database;
    var result = db.delete(
      expenseTable,
      where: '$columnId = ?',
      whereArgs: [id],
    );

    return result;
  }

  Future deleteAllAmount() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      expenseTable,
    );
    return result;
  }
}
