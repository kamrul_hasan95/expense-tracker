import 'dart:async';
import 'dart:io';

import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final String expenseTable = 'ExpenseTracker';

class DatabaseProvider {
  static final DatabaseProvider dbProvider = DatabaseProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'ExpenseTracker.db');

    var database = await openDatabase(
      path,
      version: 1,
      onCreate: initDb,
      onUpgrade: onUpgrade,
    );

    return database;
  }

  void initDb(Database database, int version) async {
    String sqlTable = '''
        create table $expenseTable ( 
        $columnId integer primary key autoincrement, 
        $columnAmount double not null,
        $columnDay text not null,
        $columnSource text not null,
        $columnExpense integer not null)
    ''';
    await database.execute(sqlTable);
  }

  void onUpgrade(Database database, int oldVersion, int newVersion) {
    if (newVersion > oldVersion) {}
  }
}
