import 'package:flutter_expense_tracker/src/dataModels/amountModel.dart';
import 'package:flutter_expense_tracker/src/db/expenseDao.dart';

class ExpenseRepository {
  final expenseDao = ExpenseDao();

  Future getAllTodoAmounts({String query}) =>
      expenseDao.getAmounts(query: query);

  Future getTodaySpentAmount(String query) =>
      expenseDao.getTodayCalculation(query);

  Future getAllTimeSpentAmount() => expenseDao.getTotalEarnedAndExpense();

  Future insertAmount(AmountModel amountModel) =>
      expenseDao.addAmount(amountModel);

  Future updateAmount(AmountModel amountModel) =>
      expenseDao.updateAmount(amountModel);

  Future deleteAmountByID(int id) => expenseDao.deleteAmount(id);

  Future deleteAllAmount() => expenseDao.deleteAllAmount();
}
